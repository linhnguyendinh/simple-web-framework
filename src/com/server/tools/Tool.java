package com.server.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import com.server.Listener;

/**
 * @author nguyendinhlinh
 * @version 1.0.0
 */
public class Tool {

    /**
     * logger
     */
    private static Logger logger = Logger.getLogger(Listener.class.getName());

    /**
     * Read {@link InputStream} as {@link String}
     * 
     * @param in input stream
     * @return string from input stream
     */
    public static String inputStreamAsString(InputStream in) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        StringBuilder s = new StringBuilder();
        try {
            while (br.ready()) {
                s.append(br.readLine()).append('\n');
            }
            int len = s.length();
            if (len > 0) s.deleteCharAt(len-1);
        } catch (IOException e) {
            e.printStackTrace();
            logger.severe(e.getMessage());
        }
        logger.info(s.toString());
        return s.toString();
    }
}
