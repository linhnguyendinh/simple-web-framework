package com.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import com.server.tools.Tool;

/**
 * Listen request
 * 
 * @version 1.0.0
 * @author nguyendinhlinh
 */
public class Listener {
    /**
     * server port
     */
    private final static Integer PORT = 8800;
    /**
     * logger
     */
    private static Logger logger = Logger.getLogger(Listener.class.getName());

    /**
     * @param args args
     */
    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(PORT);
            logger.info(String.format("Serving HTTP on port %d ...", PORT));
            while (true) {
                Socket socket = serverSocket.accept();
                logger.info("Connected");

                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                System.out.println(Tool.inputStreamAsString(socket.getInputStream()));

                out.println("HTTP/1.1 200 OK");
                out.println("Content-Type: text/html");
                out.println();
                out.println("Hello, World!");
                out.flush();
                out.close();
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
            logger.severe(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            logger.severe(e.getMessage());
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
                logger.severe(e.getMessage());
            }
        }
    }
}
