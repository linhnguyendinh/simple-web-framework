package com.server;

import java.util.Map;

/**
 * @author nguyendinhlinh
 * @version 1.0.0
 */
public abstract class Header {
    /**
     * Host
     */
    private String host;
    /**
     * 
     */
    private String connection;
    /**
     * 
     */
    private int contentLength;
    /**
     * 
     */
    private Map<String, Object> cacheControl;
    /**
     * 
     */
    private int upgradeInsecureRequests;
    /**
     * 
     */
    private String origin;
    /**
     * 
     */
    private String contentType;
    /**
     * 
     */
    private String userAgent;
    /**
     * 
     */
    private String accept;
    /**
     * 
     */
    private String acceptEncoding;
    /**
     * 
     */
    private String acceptLanguage;
}
