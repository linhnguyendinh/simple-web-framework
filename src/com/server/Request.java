package com.server;

import java.io.InputStream;

/**
 * @author nguyendinhlinh
 * @version 1.0.0
 */
public abstract class Request {
    /**
     * Request {@link InputStream}
     */
    private InputStream requestIn;
    /**
     * Request path
     */
    private String path;
    /**
     * Method: <i>POST, GET...</i>
     */
    private String method;
    /**
     * Http header
     */
    private Header header;
}
